<?php
 
require_once 'include/DB_Functions.php';
$db = new DB_Functions();
 
// json response array
$response = array("error" => FALSE);
 
if (isset($_POST['user_name']) && isset($_POST['emailId']) && isset($_POST['pass']) && isset($_POST['mobile_no'])) {
 
    // receiving the post params
    $name = $_POST['user_name'];
    $email = $_POST['emailId'];
    $password = $_POST['pass'];
	$mobile = $_POST['mobile_no'];
 
    // check if user is already existed with the same email
    if ($db->isUserExisted($email)) {
        // user already existed
        $response["error"] = TRUE;
        $response["error_msg"] = "User already existed with " . $email;
        echo json_encode($response);
    } else {
        // create a new user
        $user = $db->storeUser($name, $email, $password, $mobile);
        if ($user) {
            // user stored successfully
            $response["error"] = FALSE;
            //$response["uid"] = $user["unique_id"];
            $response["user"]["name"] = $user["user_name"];
            $response["user"]["email"] = $user["emailId"];
			$response["user"]["mobile_no"] = $user["mobile_no"];
            $response["user"]["created_at"] = $user["created_at"];
            $response["user"]["updated_at"] = $user["updated_at"];
            echo json_encode($response);
        } else {
            // user failed to store
            $response["error"] = TRUE;
            $response["error_msg"] = "Unknown error occurred in registration!";
            echo json_encode($response);
        }
    }
} else {
    $response["error"] = TRUE;
    $response["error_msg"] = "Required parameters (name, email, mobile or password) is missing!";
    echo json_encode($response);
}
?>