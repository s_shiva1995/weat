<?php 
require_once 'include/DB_Functions.php';
$db = new DB_Functions();
 
// json response array
$response = array("error" => FALSE);
if(isset($_POST["emailId"])){
	$email = $_POST["emailId"];
	$contacts = $db->getUserEmergencyContact($email);
	if($contacts){
		$response["error"] = FALSE;
        //$response["uid"] = $user["unique_id"];
        $response["contacts"]["mob1"] = $contacts["mobile_no1"];
        $response["contacts"]["mob2"] = $contacts["mobile_no2"];
		$response["contacts"]["mob3"] = $contacts["mobile_no3"];
        $response["contacts"]["mob4"] = $contacts["mobile_no4"];
        $response["contacts"]["mob5"] = $contacts["mobile_no5"];
        echo json_encode($response);
	}else{
		echo "Failed";
	}
	
}
?>