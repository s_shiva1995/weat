package com.mukta.shivansh.weat;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.view.WindowManager;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class loadHandler extends AppCompatActivity implements AsyncResponse{

    String type, email, message;
    BackgroundWorker backgroundWorker;
    SharedPreferences sharedPreferences;
    Intent i;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_load_handler);

        backgroundWorker = new BackgroundWorker(loadHandler.this);
        backgroundWorker.delegate = this;

        sharedPreferences = getSharedPreferences("myPreference", Context.MODE_PRIVATE);

        Intent intent = getIntent();
        type = intent.getStringExtra("type");
        email = intent.getStringExtra("email");

        switch (type){
            case "login":
                //String email = intent.getStringExtra("email");
                String pass = intent.getStringExtra("pass");
                backgroundWorker.execute(type, email, pass);
                break;
            case "register":
                String name = intent.getStringExtra("name");
                String password = intent.getStringExtra("pass");
                String mob = intent.getStringExtra("mob");
                //String emailID = intent.getStringExtra("email");
                backgroundWorker.execute(type, name, password, mob, email);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString("userName", name);
                editor.putString("userEmail", email);
                editor.putString("userMobile", mob);
                editor.apply();
                editor.commit();
                i = new Intent(this, emergencyContacts.class);
                i.putExtra("emailId", email);
                break;
            case "emergency":
                //String emailId = intent.getStringExtra("email");
                String num1 = intent.getStringExtra("num1");
                String num2 = intent.getStringExtra("num2");
                String num3 = intent.getStringExtra("num3");
                String num4 = intent.getStringExtra("num4");
                String num5 = intent.getStringExtra("num5");
                backgroundWorker.execute(type, email, num1, num2, num3, num4, num5);
                break;
            case "SOS":
                message = "Sent using WEAT: " + intent.getStringExtra("name_sos") + " is nearby "
                        + intent.getStringExtra("location") + " mobile number "
                        + intent.getStringExtra("mobile_sos")
                        + " Message: " + intent.getStringExtra("msg_sos") + " Track at: "  + "http://maps.google.com/?q="
                        + intent.getStringExtra("latitude") + "," + intent.getStringExtra("longitude");
                backgroundWorker.execute(type, email);
                break;
        }
    }

    @Override
    public void onProcessFinish(String output) {
        switch(type){
            case "login":
                //Toast.makeText(this, output, Toast.LENGTH_LONG).show();
                try{
                    JSONObject jsonObject = new JSONObject(output);
                    String error = jsonObject.getString("error");
                    //Toast.makeText(this, error, Toast.LENGTH_SHORT).show();
                    if(error.equals("true")){
                        String error_msg = jsonObject.getString("error_msg");
                        startActivity(new Intent(this, loginActivity.class));
                        Toast.makeText(this, error_msg, Toast.LENGTH_LONG).show();
                        finish();
                    }else{
                        JSONObject jsonObject1 = jsonObject.getJSONObject("user");
                        String name = jsonObject1.getString("name");
                        String email = jsonObject1.getString("email");
                        String mobile_no = jsonObject1.getString("mobile_no");
                        //Toast.makeText(this, name + " " + email + " " + mobile_no, Toast.LENGTH_SHORT).show();
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.putString("userName", name);
                        editor.putString("userEmail", email);
                        editor.putString("userMobile", mobile_no);
                        editor.apply();
                        editor.commit();
                        startActivity(new Intent(this, sosActivity.class));
                        finish();
                    }
                }catch (JSONException j){}
                break;
            case "register":
                startActivity(i);
                finish();
                break;
            case "emergency":
                startActivity(new Intent(this, sosActivity.class));
                finish();
                break;
            case "SOS":
                try {
                    JSONObject jsonObject = new JSONObject(output);
                    JSONObject contact = jsonObject.getJSONObject("contacts");
                    for(int i = 0; i < contact.length(); i++){
                        String variableName = "mob" + Integer.toString(i+1);
                        final String mobile = contact.getString(variableName);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                SmsManager smsManager = SmsManager.getDefault();
                                String SENT = "SMS_SENT";
                                String DELIVERED = "SMS_DELIVERED";
                                String number = "+91" + mobile;

                                PendingIntent sentPI = PendingIntent.getBroadcast(loadHandler.this, 0,
                                        new Intent(SENT), 0);

                                PendingIntent deliveredPI = PendingIntent.getBroadcast(loadHandler.this, 0,
                                        new Intent(DELIVERED), 0);

                                ArrayList<String> parts = smsManager.divideMessage(message);
                                ArrayList<PendingIntent> sendList = new ArrayList<>();
                                sendList.add(sentPI);
                                ArrayList<PendingIntent> deliverList = new ArrayList<>();
                                deliverList.add(deliveredPI);

                                smsManager.sendMultipartTextMessage(number, null, parts, sendList, deliverList);
                            }
                        });
                    }

                }catch (JSONException e){e.printStackTrace();}
                startActivity(new Intent(this, sosActivity.class));
                Toast.makeText(this, "Messages Sent", Toast.LENGTH_LONG).show();
                finish();
                break;
        }
    }
}
