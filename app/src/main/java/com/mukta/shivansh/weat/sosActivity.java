package com.mukta.shivansh.weat;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class sosActivity extends AppCompatActivity implements AsyncResponse {

    SharedPreferences sharedPreferences;
    TextView txtName, txtEmail, txtMobile, txtLoc, txtLatitde, txtLongitude;
    Button btnLogout, btnSOS;
    EditText etMsg;
    Handler handler;
    Runnable runnable;
    BackgroundWorker backgroundWorker;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_sos, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){
            case R.id.about:
                startActivity(new Intent(this, about.class));
                return true;
            case R.id.settings:
                Toast.makeText(this, "Settings Pressed", Toast.LENGTH_SHORT).show();
                return true;
            case R.id.logout:
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.clear();
                editor.apply();
                editor.commit();
                startActivity(new Intent(sosActivity.this, loginActivity.class));
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sos);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        txtName = (TextView) findViewById(R.id.txtName);
        txtMobile = (TextView) findViewById(R.id.txtMobile);
        txtEmail = (TextView) findViewById(R.id.txtEmail);
        txtLoc = (TextView) findViewById(R.id.txtLoc);
        txtLatitde = (TextView) findViewById(R.id.txtLatitude);
        txtLongitude = (TextView) findViewById(R.id.txtLongitude);

        btnLogout = (Button) findViewById(R.id.btnLogout);
        btnSOS = (Button) findViewById(R.id.btnSOS);

        etMsg = (EditText) findViewById(R.id.etMsg);

        sharedPreferences = getSharedPreferences("myPreference", Context.MODE_PRIVATE);
        txtName.setText(sharedPreferences.getString("userName", null));
        txtEmail.setText(sharedPreferences.getString("userEmail", null));
        txtMobile.setText(sharedPreferences.getString("userMobile", null));

        if(txtEmail.equals(null)){
            startActivity(new Intent(this, loginActivity.class));
            finish();
        }

        backgroundWorker = new BackgroundWorker(sosActivity.this);
        backgroundWorker.delegate = this;

        handler = new Handler();
        runnable = new Runnable() {
        @Override
        public void run() {
            GPSTracker gpsTracker = new GPSTracker(sosActivity.this);
            if(gpsTracker.getIsGPSTrackingEnabled()) {
                String address = String.valueOf(gpsTracker.getAddressLine(sosActivity.this));
                /*String locality = String.valueOf(gpsTracker.getLocality(sosActivity.this));
                String state = String.valueOf(gpsTracker.getState(sosActivity.this));
                String country = String.valueOf(gpsTracker.getCountryName(sosActivity.this));
                String postal = String.valueOf(gpsTracker.getPostalCode(sosActivity.this));*/
                double latitude = gpsTracker.getLatitude();
                double longitude = gpsTracker.getLongitude();
                txtLoc.setText(address);
                txtLongitude.setText(""+longitude);
                txtLatitde.setText(""+latitude);
                handler.postDelayed(this, 5000);
            }else
                gpsTracker.showSettingsAlert();
            }
        };
        handler.post(runnable);

        btnLogout.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.clear();
            editor.apply();
            editor.commit();
            startActivity(new Intent(sosActivity.this, loginActivity.class));
            finish();
            }
        });

        btnSOS.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email = txtEmail.getText().toString();
                String type = "SOS";
                Intent i = new Intent(sosActivity.this, loadHandler.class);
                i.putExtra("type", type);
                i.putExtra("email", email);
                i.putExtra("name_sos", txtName.getText().toString());
                i.putExtra("latitude", txtLatitde.getText().toString());
                i.putExtra("longitude", txtLongitude.getText().toString());
                i.putExtra("location", txtLoc.getText().toString());
                i.putExtra("mobile_sos", txtMobile.getText().toString());
                i.putExtra("msg_sos", etMsg.getText().toString());
                startActivity(i);
                //backgroundWorker.execute(type, email);

            }
        });
    }

    @Override
    protected void onStop() {
        handler.removeCallbacks(runnable);
        super.onStop();
    }

    @Override
    protected void onResume() {
        handler.post(runnable);
        super.onResume();
    }

    @Override
    protected void onRestart() {
        handler.post(runnable);
        super.onRestart();
    }

    @Override
    public void onProcessFinish(String output) {
        /*try {
            JSONObject jsonObject = new JSONObject(output);
            JSONObject contact = jsonObject.getJSONObject("contacts");
            for(int i = 0; i < contact.length(); i++){
                String variableName = "mob" + Integer.toString(i+1);
                final String mobile = contact.getString(variableName);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        SmsManager smsManager = SmsManager.getDefault();
                        String SENT = "SMS_SENT";
                        String DELIVERED = "SMS_DELIVERED";
                        String number = "+91" + mobile;

                        PendingIntent sentPI = PendingIntent.getBroadcast(sosActivity.this, 0,
                                new Intent(SENT), 0);

                        PendingIntent deliveredPI = PendingIntent.getBroadcast(sosActivity.this, 0,
                                new Intent(DELIVERED), 0);

                        String message = "Sent using WEAT: " + txtName.getText().toString() + " is nearby "
                                + txtLoc.getText().toString() + " mobile number "
                                + txtMobile.getText().toString()
                                + " Message: " + etMsg.getText().toString();
                        ArrayList<String> parts = smsManager.divideMessage(message);
                        ArrayList<PendingIntent> sendList = new ArrayList<>();
                        sendList.add(sentPI);
                        ArrayList<PendingIntent> deliverList = new ArrayList<>();
                        deliverList.add(deliveredPI);

                        smsManager.sendMultipartTextMessage(number, null, parts, sendList, deliverList);
                    }
                });
            }
            etMsg.setText("");

        }catch (JSONException e){e.printStackTrace();}*/
    }
}


