package com.mukta.shivansh.weat;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class loginActivity extends AppCompatActivity implements AsyncResponse{

    EditText etLogin, etPass;
    Button btnLogin;
    //SharedPreferences sharedPreferences;
    BackgroundWorker backgroundWorker;
    Intent intent;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){
            case R.id.about:
                startActivity(new Intent(this, about.class));
                return true;
            case R.id.settings:
                startActivity(new Intent(this, settings.class));
                return true;
            case R.id.register:
                startActivity(new Intent(this, first_Screen_reg.class));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        etLogin = (EditText) findViewById(R.id.userEmail);
        etPass = (EditText) findViewById((R.id.userPass));

        btnLogin = (Button) findViewById(R.id.btnLogin);

        backgroundWorker = new BackgroundWorker(loginActivity.this);
        backgroundWorker.delegate = this;

        intent = new Intent(this, loadHandler.class);

        btnLogin.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                String email = etLogin.getText().toString();
                String pass = etPass.getText().toString();
                String type = "";
                if(email.equals("")){
                    etLogin.setError("Empty Field");
                }else if(pass.equals("")){
                    etPass.setError("");
                }else if(!isValidEmail(email)){
                    etLogin.setError("Invalid Email");
                }else
                    type = "login";
                if(type.equals("login")) {
                    //new BackgroundWorker(loginActivity.this).execute(type, email, pass);
                    //backgroundWorker.execute(type, email, pass);
                    intent.putExtra("email", email);
                    intent.putExtra("pass", pass);
                    intent.putExtra("type", type);
                    startActivity(intent);
                    finish();
                }
            }
        });
    }

    public boolean isValidEmail(String email){
        String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    @Override
    public void onProcessFinish(String output) {
        //Toast.makeText(this, output, Toast.LENGTH_LONG).show();
        /*try{
            JSONObject jsonObject = new JSONObject(output);
            JSONObject error =  jsonObject.getJSONObject("error");
            if(error.equals("false")){
                /*SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString("userName", etLogin.getText().toString());
                //startActivity(new Intent(this, sosActivity.class));
                Toast.makeText(this, "Valid",Toast.LENGTH_LONG).show();
            }else
                Toast.makeText(this, "Invalid Email ID or Password", Toast.LENGTH_LONG).show();
        }catch (JSONException j){

        }*/
    }
}
