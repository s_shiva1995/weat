package com.mukta.shivansh.weat;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.BufferedReader;

public class emergencyContacts extends AppCompatActivity implements AsyncResponse{

    Button btnNext;
    EditText etNum1, etNum2, etNum3, etNum4, etNum5;
    BackgroundWorker backgroundWorker;

    /*@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_weat, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){
            case R.id.about:
                Toast.makeText(this, "About Pressed", Toast.LENGTH_SHORT).show();
                return true;
            case R.id.settings:
                Toast.makeText(this, "Settings Pressed", Toast.LENGTH_SHORT).show();
                return true;
            case R.id.register:
                startActivity(new Intent(this, first_Screen_reg.class));
                return true;
            case R.id.login:
                startActivity(new Intent(this, loginActivity.class));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }*/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_emergency_contacts);
        btnNext = (Button) findViewById(R.id.btnNext);
        etNum1 = (EditText) findViewById(R.id.etNum1);
        etNum2 = (EditText) findViewById(R.id.etNum2);
        etNum3 = (EditText) findViewById(R.id.etNum3);
        etNum4 = (EditText) findViewById(R.id.etNum4);
        etNum5 = (EditText) findViewById(R.id.etNum5);

        SharedPreferences sharedPreferences = getSharedPreferences("myPreference", Context.MODE_PRIVATE);
        //SharedPreferences.Editor editor = sharedPreferences.edit();
        String name = sharedPreferences.getString("userName", null);

        backgroundWorker = new BackgroundWorker(emergencyContacts.this);
        backgroundWorker.delegate = this;

        /*if(name.equals(""))
            finish();*/

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String type = "";
                Intent i = getIntent();
                String email = i.getExtras().getString("emailId");

                //comment for validations

                if(etNum1.equals(""))
                    etNum1.setError("3 Contacts Mandatory");
                else if(etNum2.equals(""))
                    etNum2.setError("3 Contacts Mandatory");
                else if(etNum3.equals(""))
                    etNum3.setError("3 Contacts Mandatory");
                else
                    type = "emergency";
                Intent intent = new Intent(emergencyContacts.this, loadHandler.class);
                intent.putExtra("type", type);
                intent.putExtra("email", email);
                intent.putExtra("num1", etNum1.getText().toString());
                intent.putExtra("num2", etNum2.getText().toString());
                intent.putExtra("num3", etNum3.getText().toString());
                intent.putExtra("num4", etNum4.getText().toString());
                intent.putExtra("num5", etNum5.getText().toString());
                startActivity(intent);
                finish();
                /*backgroundWorker.execute(type, email,etNum1.getText().toString(), etNum2.getText().toString(),
                        etNum3.getText().toString(), etNum4.getText().toString(), etNum5.getText().toString());
                startActivity(new Intent(emergencyContacts.this, sosActivity.class));
                finish();*/
            }
        });
    }

    @Override
    public void onProcessFinish(String output) {

    }
}
