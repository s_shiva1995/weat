package com.mukta.shivansh.weat;

/**
 * Created by shivansh on 2/17/2017.
 */

public interface AsyncResponse {
    void onProcessFinish(String output);
}
