package com.mukta.shivansh.weat;

import android.app.AlertDialog;
import android.content.Context;
import android.os.AsyncTask;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;

/**
 * Created by shivansh on 2/13/2017.
 */

public class BackgroundWorker extends AsyncTask<String, Void, String> {

    Context context;
    AlertDialog alertDialog;
    String type;

    public AsyncResponse delegate = null;

    BackgroundWorker(Context ctx){
        context = ctx;
    }

    @Override
    protected String doInBackground(String... params) {
        type = params[0];
        switch (type) {
            case "login":
                String loginURL = "http://192.168.43.93/weat/login.php";
            try {
                String email = params[1];
                String pass = params[2];
                String postData = URLEncoder.encode("email", "UTF-8") + "=" + URLEncoder.encode(email, "UTF-8") + "&"
                        + URLEncoder.encode("pass", "UTF-8") + "=" + URLEncoder.encode(pass, "UTF-8");
                String result = queryExecutor(loginURL, postData);
                return result;
            } catch (IOException e) {
                e.printStackTrace();
            }
                break;
            case "register":
            try {
                String registerUrl = "http://192.168.43.93/weat/register.php";
                String name = params[1];
                String email = params[4];
                String password = params[2];
                String mobile = params[3];
                String postData = URLEncoder.encode("user_name", "UTF-8") + "=" + URLEncoder.encode(name, "UTF-8") + "&"
                        + URLEncoder.encode("emailId", "UTF-8") + "=" + URLEncoder.encode(email, "UTF-8") + "&"
                        + URLEncoder.encode("pass", "UTF-8") + "=" + URLEncoder.encode(password, "UTF-8") + "&"
                        + URLEncoder.encode("mobile_no", "UTF-8") + "=" + URLEncoder.encode(mobile, "UTF-8");
                String result = queryExecutor(registerUrl, postData);
                return result;
            } catch (IOException e) {
                e.printStackTrace();
            }
                break;
            case "emergency":
            try {
                String emergencyUrl = "http://192.168.43.93/weat/emergency.php";
                String email = params[1];
                String mob1 = params[2];
                String mob2 = params[3];
                String mob3 = params[4];
                String mob4 = params[5];
                String mob5 = params[6];
                String postData = URLEncoder.encode("emailId", "UTF-8") + "=" + URLEncoder.encode(email, "UTF-8") + "&"
                        + URLEncoder.encode("mobile_no1", "UTF-8") + "=" + URLEncoder.encode(mob1, "UTF-8") + "&"
                        + URLEncoder.encode("mobile_no2", "UTF-8") + "=" + URLEncoder.encode(mob2, "UTF-8") + "&"
                        + URLEncoder.encode("mobile_no3", "UTF-8") + "=" + URLEncoder.encode(mob3, "UTF-8") + "&"
                        + URLEncoder.encode("mobile_no4", "UTF-8") + "=" + URLEncoder.encode(mob4, "UTF-8") + "&"
                        + URLEncoder.encode("mobile_no5", "UTF-8") + "=" + URLEncoder.encode(mob5, "UTF-8");
                String result = queryExecutor(emergencyUrl, postData);
                return result;
            } catch (IOException e) {
                e.printStackTrace();
            }
                break;
            case "SOS":
            try{
                String sosURL = "http://192.168.43.93/weat/sos.php";
                String email = params[1];
                String postData = URLEncoder.encode("emailId", "UTF-8") + "=" + URLEncoder.encode(email, "UTF-8");
                String result = queryExecutor(sosURL, postData);
                return result;
            }catch (IOException e){
                e.printStackTrace();
            }
                break;
            case "registerToken":
                try {
                    String tokenURL = "http://192.168.43.93/weat/fcmTokenRegister.php";
                    String postData = URLEncoder.encode("emailId", "UTF-8") + "=" + URLEncoder.encode(params[2], "UTF-8")
                            + "&" + URLEncoder.encode("token", "UTF-8") + "=" + URLEncoder.encode(params[1], "UTF-8");
                    String result = queryExecutor(tokenURL, postData);
                    return result;
                }catch (IOException e){e.printStackTrace();}
                break;
        }
        return null;
    }

    @Override
    protected void onPreExecute() {
        alertDialog = new AlertDialog.Builder(context).create();
        alertDialog.setTitle("Status");
    }

    @Override
    protected void onPostExecute(String result) {
        alertDialog.setMessage(result);
        switch (type){
            case "SOS":
                //context.startActivity(new Intent(context, sosActivity.class));
                break;
            case "emergency":
                break;
            case "login":
                //Toast.makeText(context, result, Toast.LENGTH_LONG).show();
                //context.startActivity(new Intent(context, sosActivity.class));
                break;
            case "register":
                break;
        }
        delegate.onProcessFinish(result);
        //alertDialog.show();
    }

    @Override
    protected void onProgressUpdate(Void... values) {
        super.onProgressUpdate(values);
    }

    public String queryExecutor(String URL, String postData){
        try {
            URL url = new URL(URL);
            HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
            httpURLConnection.setRequestMethod("POST");
            httpURLConnection.setDoOutput(true);
            httpURLConnection.setDoInput(true);
            OutputStream outputStream = httpURLConnection.getOutputStream();
            BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"));
            bufferedWriter.write(postData);
            bufferedWriter.flush();
            bufferedWriter.close();
            outputStream.close();
            InputStream inputStream = httpURLConnection.getInputStream();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "iso-8859-1"));
            String result = "";
            String line;
            while ((line = bufferedReader.readLine()) != null){
                result += line;
            }
            bufferedReader.close();
            inputStream.close();
            httpURLConnection.disconnect();
            return result;
        }
        catch (MalformedURLException m){
            m.printStackTrace();
        }
        catch (IOException e){
            e.printStackTrace();
        }
        return null;
    }
}
