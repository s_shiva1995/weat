package com.mukta.shivansh.weat;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class first_Screen_reg extends AppCompatActivity implements AsyncResponse{

    Button btnNext, btnLogin;
    EditText userName, userPassword, userConfirmPassword, userEmail, userMobile;
    SharedPreferences sharedPreferences;
    String email;
    BackgroundWorker backgroundWorker;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_weat, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){
            case R.id.about:
                startActivity(new Intent(this, about.class));
                return true;
            case R.id.settings:
                startActivity(new Intent(this, settings.class));
                return true;
            case R.id.login:
                startActivity(new Intent(this, loginActivity.class));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first__screen_reg);

        btnNext = (Button) findViewById(R.id.btnNext);
        btnLogin = (Button) findViewById(R.id.btnLogin);

        userName = (EditText) findViewById(R.id.userName);
        userPassword = (EditText) findViewById(R.id.userPassword);
        userConfirmPassword = (EditText) findViewById(R.id.userConfirmPssword);
        userEmail = (EditText) findViewById(R.id.userEmail);
        userMobile = (EditText) findViewById(R.id.userMobile);

        backgroundWorker = new BackgroundWorker(first_Screen_reg.this);
        backgroundWorker.delegate = this;

        sharedPreferences = getSharedPreferences("myPreference", Context.MODE_PRIVATE);
        String name = sharedPreferences.getString("userName", "");
        if(!name.equals("")){
            startActivity(new Intent(this, sosActivity.class));
            finish();
        }

        btnLogin.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent i = new Intent(first_Screen_reg.this, loginActivity.class);
                startActivity(i);
            }
        });

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String name = userName.getText().toString();
                String pass = userPassword.getText().toString();
                String cpass = userConfirmPassword.getText().toString();
                String mob = userMobile.getText().toString();
                email = userEmail.getText().toString();
                String type = "";

                //comment for validations

                if(name.equals("")){
                    userName.setError("Empty Field");
                }else if(pass.equals("")){
                    userPassword.setError("Empty Field");
                }else if(cpass.equals("")){
                    userConfirmPassword.setError("Empty Field");
                }else if(email.equals("")){
                    userEmail.setError("Empty Field");
                }else if(mob.equals("")){
                    userMobile.setError("Empty Field");
                }else{
                    if(pass.equals(cpass)){
                        if(!isValidEmail(email)) {
                            userEmail.setError("Invalid Email");
                        }else
                            if(mob.length() != 10)
                                userMobile.setError("Invalid Number");
                            else
                                type = "register";
                    }else{
                        userPassword.setError("Password not Match");
                    }
                }

                if(type.equals("register")) {
                    Intent i = new Intent(first_Screen_reg.this, loadHandler.class);
                    i.putExtra("type", type);
                    i.putExtra("name", name);
                    i.putExtra("pass", pass);
                    i.putExtra("mob", mob);
                    i.putExtra("email", email);
                    startActivity(i);
                    finish();
                    /*backgroundWorker.execute(type, name, pass, mob, email);
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putString("userName", name);
                    editor.putString("userEmail", email);
                    editor.putString("userMobile", mob);
                    editor.apply();
                    editor.commit();
                    Intent i = new Intent(first_Screen_reg.this, emergencyContacts.class);
                    i.putExtra("emailId", email);
                    startActivity(i);
                    finish();*/
                }
            }
        });
    }

    public boolean isValidEmail(String email){
        String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    @Override
    public void onProcessFinish(String output) {
        Toast.makeText(this, output, Toast.LENGTH_LONG).show();
        /*Intent i = new Intent(first_Screen_reg.this, emergencyContacts.class);
        i.putExtra("emailId", email);
        startActivity(i);
        finish();*/
    }
}
