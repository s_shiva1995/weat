<?php
require_once 'include/DB_Functions.php';
$db = new DB_Functions();
 
// json response array
$response = array("error" => FALSE);
 
if (isset($_POST['email']) && isset($_POST['pass'])) {
 
    // receiving the post params
    $email = $_POST['email'];
    $password = $_POST['pass'];
	//$user = true;
    // get the user by email and password
    $user = $db->getUserByEmailAndPassword($email, $password);
 
    if ($user != false) {
        // user is found
        $response["error"] = FALSE;
        //$response["uid"] = $user["unique_id"];
        $response["user"]["name"] = $user["user_name"];
        $response["user"]["email"] = $user["emailId"];
		$response["user"]["mobile_no"] = $user["mobile_no"];
        $response["user"]["created_at"] = $user["created_at"];
        $response["user"]["updated_at"] = $user["updated_at"];
        echo json_encode($response);
		//echo "logged in";
    } else {
        // user is not found with the credentials
        $response["error"] = TRUE;
        $response["error_msg"] = "Login credentials are wrong. Please try again!";
        echo json_encode($response);
		//echo "log in not success";
    }
} else {
    // required post params is missing
    $response["error"] = TRUE;
    $response["error_msg"] = "Required parameters email or password is missing!";
    echo json_encode($response);
	//echo "Invalid credentials";
}
?>