<?php

class DB_Functions {
 
    private $conn;
 
    // constructor
    function __construct() {
        require_once 'DB_Connect.php';
        // connecting to database
        $db = new Db_Connect();
        $this->conn = $db->connect();
    }
 
    // destructor
    function __destruct() {
         
    }
 
    /**
     * Storing new user
     * returns user details
     */
    public function storeUser($name, $email, $password, $mobile_no) {
        //$uuid = uniqid('', true);
        //$hash = $this->hashSSHA($password);
        $encrypted_password = password_hash($password, PASSWORD_DEFAULT); // encrypted password
		$salt = "l";
        //$salt = $hash["salt"]; // salt
 
        $stmt = $this->conn->prepare("INSERT INTO user_reg(user_name, emailId, pass, salt, mobile_no, created_at) VALUES(?, ?, ?, ?, ?, NOW())");
        $stmt->bind_param("sssss", $name, $email, $encrypted_password, $salt, $mobile_no);
        $result = $stmt->execute();
        $stmt->close();
 
        // check for successful store
        if ($result) {
            $stmt = $this->conn->prepare("SELECT * FROM user_reg WHERE emailId = ?");
            $stmt->bind_param("s", $email);
            $stmt->execute();
            $user = $stmt->get_result()->fetch_assoc();
            $stmt->close();
 
            return $user;
        } else {
            return false;
        }
    }
 
    /**
     * Get user by email and password
     */
    public function getUserByEmailAndPassword($email, $password) {
 
        $stmt = $this->conn->prepare("SELECT * FROM user_reg WHERE emailID = ?");
 
        $stmt->bind_param("s", $email);
 
        if ($stmt->execute()) {
            $user = $stmt->get_result()->fetch_assoc();
            $stmt->close();
 
            // verifying user password
            //$salt = $user['salt'];
            $encrypted_password = $user['pass'];
			
            $hash = password_verify($password, $encrypted_password);//$this->checkhashSSHA($salt, $encrypted_password);
			
            // check for password equality
            if ($encrypted_password == $hash) {
                // user authentication details are correct
                return $user;
				//return true;
            }
        } else {
            return NULL;
        }
    }
 
    /**
     * Check user is existed or not
     */
    public function isUserExisted($email) {
        $stmt = $this->conn->prepare("SELECT emailId from user_reg WHERE emailId = ?");
 
        $stmt->bind_param("s", $email);
 
        $stmt->execute();
 
        $stmt->store_result();
 
        if ($stmt->num_rows > 0) {
            // user existed 
            $stmt->close();
            return true;
        } else {
            // user not existed
            $stmt->close();
            return false;
        }
    }
 
    /**
     * Encrypting password
     * @param password
     * returns salt and encrypted password
     */
    public function hashSSHA($password) {
 
        $salt = sha1(rand());
        $salt = substr($salt, 0, 10);
        $encrypted = base64_encode(sha1($password . $salt, true) . $salt);
        $hash = array("salt" => $salt, "encrypted" => $encrypted);
        return $hash;
    }
 
    /**
     * Decrypting password
     * @param salt, password
     * returns hash string
     */
    public function checkhashSSHA($salt, $password) {
 
        $hash = base64_encode(sha1($password . $salt, true) . $salt);
 
        return $hash;
    }
	
	public function emergency($email, $mob1, $mob2, $mob3, $mob4, $mob5){
		$stmt = $this->conn->prepare("insert into emergencycontact(emailId, mobile_no1, mobile_no2, mobile_no3, mobile_no4, mobile_no5) values(?,?,?,?,?,?)");
		$stmt->bind_param("ssssss", $email, $mob1, $mob2, $mob3, $mob4, $mob5);
		$result = $stmt->execute();
		$stmt->close();
		return $result;
	}
	
	public function registerToken($email, $token){
		$stmt = $this->conn->prepare("insert into fcmtoken(emailId, token) values(?,?) on duplicate key update token = ?");// 
		$stmt->bind_param("sss", $email, $token, $token);
		$result = $stmt->execute();
		$stmt->close();
		return $result;
	}
	
	public function getUserEmergencyContact($email){
		$stmt = $this->conn->prepare("select * from emergencycontact where emailId = ?");
		$stmt->bind_param("s", $email);
		if($stmt->execute()){
			$user = $stmt->get_result()->fetch_assoc();
			$stmt->close();
			return $user;
		}else
			return NULL;
	}
 
}
 
?>